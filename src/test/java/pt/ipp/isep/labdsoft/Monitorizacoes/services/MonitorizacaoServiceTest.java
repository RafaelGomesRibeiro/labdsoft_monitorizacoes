package pt.ipp.isep.labdsoft.Monitorizacoes.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.persistence.PedidoMonitorizacaoRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class MonitorizacaoServiceTest {

    @Autowired
    private PedidoMonitorizacaoRepository pedidoMonitorizacaoRepository;
    @Test
    public void ensurePedidoMonitorizacaoIsCreatedWithRealizadaAsFalse() {
        PedidoMonitorizacaoServiceImpl pedidoMonitorizacaoService = new PedidoMonitorizacaoServiceImpl(pedidoMonitorizacaoRepository);
        PedidoMonitorizacaoDTO dto = PedidoMonitorizacaoDTO.builder().data("10-10-2010 10:10")
                .descricao("teste").nome("Sim").medicoId(1L).numPaciente(2L).build();
        PedidoMonitorizacaoDTO actualDTO = pedidoMonitorizacaoService.save(dto);
        Assertions.assertFalse(actualDTO.realizado, "O pedido não iniciou com o realizado a false");

    }
}
