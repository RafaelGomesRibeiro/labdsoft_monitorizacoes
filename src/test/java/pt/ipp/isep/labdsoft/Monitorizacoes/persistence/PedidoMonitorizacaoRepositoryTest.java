package pt.ipp.isep.labdsoft.Monitorizacoes.persistence;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.PedidoMonitorizacao;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;


@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class PedidoMonitorizacaoRepositoryTest {

    @Autowired
    private PedidoMonitorizacaoRepository pedidoMonitorizacaoRepository;

    @Test
    public void ensureMonitorizacaoIsCorrectlySaved(){
        PedidoMonitorizacao expectedValue = PedidoMonitorizacao.builder().data(LocalDateTime.now()).descricao("pedido 1")
                .nome("Sim senhor").numPaciente(1L).medicoId(1L).realizado(false).build();
        expectedValue = pedidoMonitorizacaoRepository.save(expectedValue);
        PedidoMonitorizacao actualValue = pedidoMonitorizacaoRepository.findById(expectedValue.getId()).orElseThrow(() -> new NoSuchElementException("Pedido não foi guardado na base de dados corretamente"));
        Assertions.assertEquals(expectedValue, actualValue, "Os pedidos não são iguais");
    }

}
