package pt.ipp.isep.labdsoft.Monitorizacoes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.MonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.services.MonitorizacaoService;

import java.util.List;


@RestController
@RequestMapping("monitorizacoes")

public final class MonitorizacaoController {

    @Autowired
    private MonitorizacaoService monitorizacaoService;


    @GetMapping("/")
    public ResponseEntity<List<MonitorizacaoDTO>> monitorizacoes() {
        List<MonitorizacaoDTO> monitorizacoes = monitorizacaoService.listAll();
        return new ResponseEntity<>(monitorizacoes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MonitorizacaoDTO> byId(@PathVariable Long id) {
        MonitorizacaoDTO monitorizacao = monitorizacaoService.byID(id);
        return new ResponseEntity<>(monitorizacao, HttpStatus.OK);
    }

    @GetMapping("/utente/{idUtente}")
    public ResponseEntity<List<MonitorizacaoDTO>> monitorizacoesUtente(@PathVariable Long idUtente) {
        List<MonitorizacaoDTO> monitorizacoes = monitorizacaoService.monitorizacoesPorUtente(idUtente);
        return new ResponseEntity<>(monitorizacoes, HttpStatus.OK);
    }



    @PostMapping("/")
    public ResponseEntity<MonitorizacaoDTO> create(@RequestBody MonitorizacaoDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                .body(monitorizacaoService.save(dto));
    }

}
