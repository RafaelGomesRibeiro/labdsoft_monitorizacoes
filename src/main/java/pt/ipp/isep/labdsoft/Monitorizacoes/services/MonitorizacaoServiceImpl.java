package pt.ipp.isep.labdsoft.Monitorizacoes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.Monitorizacao;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.PedidoMonitorizacao;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.MonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.persistence.MonitorizacaoRepository;
import pt.ipp.isep.labdsoft.Monitorizacoes.persistence.PedidoMonitorizacaoRepository;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class MonitorizacaoServiceImpl implements MonitorizacaoService {

    @Autowired
    private MonitorizacaoRepository repo;
    @Autowired
    private PedidoMonitorizacaoRepository pedidoMonitorizacaoRepository;

    @Override
    public List<MonitorizacaoDTO> listAll() {
        return repo.findAll().stream().map(m -> m.toDTO()).collect(Collectors.toList());
    }

    @Override
    public MonitorizacaoDTO byID(Long id) {
        Monitorizacao monitorizacao = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Monitorizacao com id " + id + " nao existe"));
        return monitorizacao.toDTO();
    }

    @Override
    public List<MonitorizacaoDTO> monitorizacoesPorUtente(Long idUtente) {
        return repo.findAll().stream().filter(m -> m.getPedido().getNumPaciente().equals(idUtente)).sorted(Comparator.comparing(Monitorizacao::getData))
                .map(Monitorizacao::toDTO).collect(Collectors.toList());
    }

    @Override
    public MonitorizacaoDTO save(MonitorizacaoDTO dto) {
        Monitorizacao monitorizacao = Monitorizacao.fromDTO(dto);
        PedidoMonitorizacao pedidoMonitorizacao = pedidoMonitorizacaoRepository.findPedidoMonitorizacaoDeUtentePorRealizar(monitorizacao.getPedido().getNumPaciente())
                .stream().findFirst().orElseThrow(() -> new NoSuchElementException("Não existe nenhum pedido de monitorização associado a este utente"));
        pedidoMonitorizacao.realizar();
        monitorizacao.setPedido(pedidoMonitorizacao);
        monitorizacao = repo.save(monitorizacao);
        pedidoMonitorizacaoRepository.save(pedidoMonitorizacao);
        return monitorizacao.toDTO();
    }
}
