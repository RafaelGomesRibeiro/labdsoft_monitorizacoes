package pt.ipp.isep.labdsoft.Monitorizacoes.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.utils.DatesFormatter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
// @Table(name = "`avaliacaoMedica`", uniqueConstraints = {@UniqueConstraint(columnNames = {"candidatura"})})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class PedidoMonitorizacao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime data;
    private Long medicoId;
    private Long numPaciente;
    private String nome;
    private String descricao;
    private Boolean realizado;


    public static PedidoMonitorizacao fromDTO(PedidoMonitorizacaoDTO dto) {
        return new PedidoMonitorizacao(dto.id, DatesFormatter.convertToLocalDateTime(dto.data), dto.medicoId, dto.numPaciente, dto.nome, dto.descricao, dto.realizado);
    }

    public PedidoMonitorizacaoDTO toDTO() {
        return new PedidoMonitorizacaoDTO(id, DatesFormatter.convertToString(data), this.medicoId, this.numPaciente, nome, descricao, realizado);
    }

    public void realizar() {
        this.realizado = true;
    }

    public boolean isRealizado() {
        return realizado;
    }
}
