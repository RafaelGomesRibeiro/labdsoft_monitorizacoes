package pt.ipp.isep.labdsoft.Monitorizacoes.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.MonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.utils.DatesFormatter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Monitorizacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long idEnfermeiro;
    private int pressaoSistolica;
    private int pressaoDiastolica;
    private int ritmoCardiaco;
    private LocalDateTime data;
    @OneToOne
    private PedidoMonitorizacao pedido;
    private String notas;

    public Monitorizacao(Long idEnfermeiro, int pressaoSistolica, int pressaoDiastolica, int ritmoCardiaco, LocalDateTime data, PedidoMonitorizacao pedido, String notas){
        this.idEnfermeiro = idEnfermeiro;
        this.pressaoSistolica = pressaoSistolica;
        this.pressaoDiastolica = pressaoDiastolica;
        this.ritmoCardiaco = ritmoCardiaco;
        this.data = data;
        this.pedido = pedido;
        this.notas = notas;
    }

    public static Monitorizacao fromDTO(MonitorizacaoDTO dto){
        PedidoMonitorizacao pedidoMonitorizacao = PedidoMonitorizacao.fromDTO(dto.pedido);
        return new Monitorizacao(dto.idEnfermeiro, dto.pressaoSistolica, dto.pressaoDiastolica, dto.ritmoCardiaco, DatesFormatter.convertToLocalDateTime(dto.data), pedidoMonitorizacao, dto.notas);
    }

    public MonitorizacaoDTO toDTO(){
        PedidoMonitorizacaoDTO pedidoMonitorizacaoDTO = this.pedido.toDTO();
        return new MonitorizacaoDTO(this.idEnfermeiro, this.pressaoSistolica, this.pressaoDiastolica, this.ritmoCardiaco, DatesFormatter.convertToString(this.data), pedidoMonitorizacaoDTO, this.notas);
    }
}
