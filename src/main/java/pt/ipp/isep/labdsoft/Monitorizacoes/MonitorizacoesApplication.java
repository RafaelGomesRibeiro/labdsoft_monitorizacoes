package pt.ipp.isep.labdsoft.Monitorizacoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitorizacoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitorizacoesApplication.class, args);
	}

}
