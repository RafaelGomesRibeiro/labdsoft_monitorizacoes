package pt.ipp.isep.labdsoft.Monitorizacoes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.services.PedidoMonitorizacaoService;

import java.util.List;


@RestController
@RequestMapping("pedidos")

public final class PedidoMonitorizacaoController {

    @Autowired
    private PedidoMonitorizacaoService pedidoMonitorizacaoService;


    @GetMapping("/")
    public ResponseEntity<List<PedidoMonitorizacaoDTO>> pedidos() {
        List<PedidoMonitorizacaoDTO> pedidos = pedidoMonitorizacaoService.listAll();
        return new ResponseEntity<>(pedidos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PedidoMonitorizacaoDTO> byId(@PathVariable Long id) {
        PedidoMonitorizacaoDTO pedido = pedidoMonitorizacaoService.byID(id);
        return new ResponseEntity<>(pedido, HttpStatus.OK);
    }

    @GetMapping("/porRealizar")
    public ResponseEntity<Iterable<PedidoMonitorizacaoDTO>> porRealizar() {
        return ResponseEntity.ok(pedidoMonitorizacaoService.pedidosPorRealizar());
    }

    @PostMapping("/")
    public ResponseEntity<PedidoMonitorizacaoDTO> create(@RequestBody PedidoMonitorizacaoDTO dto) {
        //TODO se alguem quiser
        // falta validar se o medico e o utente existem...
        PedidoMonitorizacaoDTO pedido = pedidoMonitorizacaoService.save(dto);
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(pedido);
    }
}
