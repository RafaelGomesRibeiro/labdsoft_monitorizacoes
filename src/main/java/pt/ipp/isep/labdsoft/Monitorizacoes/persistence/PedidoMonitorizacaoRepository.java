package pt.ipp.isep.labdsoft.Monitorizacoes.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.PedidoMonitorizacao;

import java.util.Collection;

@Repository
public interface PedidoMonitorizacaoRepository extends JpaRepository<PedidoMonitorizacao, Long> {
    @Query("SELECT pm FROM PedidoMonitorizacao pm where pm.realizado = false order by pm.data asc")
    Collection<PedidoMonitorizacao> findPedidoMonitorizacaoPorRealizar();
    @Query("SELECT pm FROM PedidoMonitorizacao pm where pm.realizado = false and pm.numPaciente = :numPaciente order by pm.data asc")
    Collection<PedidoMonitorizacao> findPedidoMonitorizacaoDeUtentePorRealizar(@Param("numPaciente") Long numPaciente);
}
