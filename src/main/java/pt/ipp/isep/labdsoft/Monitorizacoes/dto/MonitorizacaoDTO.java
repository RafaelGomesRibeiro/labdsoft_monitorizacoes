package pt.ipp.isep.labdsoft.Monitorizacoes.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MonitorizacaoDTO {
    public Long idEnfermeiro;
    public int pressaoSistolica;
    public int pressaoDiastolica;
    public int ritmoCardiaco;
    public String data;
    public PedidoMonitorizacaoDTO pedido;
    public String notas;
}
