package pt.ipp.isep.labdsoft.Monitorizacoes.services;

import pt.ipp.isep.labdsoft.Monitorizacoes.dto.MonitorizacaoDTO;

import java.util.List;

public interface MonitorizacaoService {

    List<MonitorizacaoDTO> listAll();
    MonitorizacaoDTO byID(Long id);
    List<MonitorizacaoDTO> monitorizacoesPorUtente(Long idUtente);
    MonitorizacaoDTO save(MonitorizacaoDTO dto);
}
