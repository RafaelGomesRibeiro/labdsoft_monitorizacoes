package pt.ipp.isep.labdsoft.Monitorizacoes.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.Monitorizacao;

@Repository
public interface MonitorizacaoRepository extends JpaRepository<Monitorizacao, Long> {
}
