package pt.ipp.isep.labdsoft.Monitorizacoes.services;

import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;

import java.util.List;

public interface PedidoMonitorizacaoService {
    List<PedidoMonitorizacaoDTO> listAll();
    PedidoMonitorizacaoDTO byID(Long id);
    PedidoMonitorizacaoDTO save(PedidoMonitorizacaoDTO dto);
    Iterable<PedidoMonitorizacaoDTO> pedidosPorRealizar();
}
