package pt.ipp.isep.labdsoft.Monitorizacoes.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class PedidoMonitorizacaoDTO {
    public Long id;
    public String data;
    public Long medicoId;
    public Long numPaciente;
    public String nome;
    public String descricao;
    public boolean realizado;
}
