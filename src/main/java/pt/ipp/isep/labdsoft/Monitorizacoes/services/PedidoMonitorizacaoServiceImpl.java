package pt.ipp.isep.labdsoft.Monitorizacoes.services;

import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Monitorizacoes.domain.PedidoMonitorizacao;
import pt.ipp.isep.labdsoft.Monitorizacoes.dto.PedidoMonitorizacaoDTO;
import pt.ipp.isep.labdsoft.Monitorizacoes.persistence.PedidoMonitorizacaoRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class PedidoMonitorizacaoServiceImpl implements PedidoMonitorizacaoService {


    private PedidoMonitorizacaoRepository repo;

    public PedidoMonitorizacaoServiceImpl(PedidoMonitorizacaoRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<PedidoMonitorizacaoDTO> listAll() {
        return repo.findAll().stream().map(PedidoMonitorizacao::toDTO).collect(Collectors.toList());
    }

    public PedidoMonitorizacaoDTO byID(Long id) {
        PedidoMonitorizacao pedido = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Pedido Monitorizacao com id " + id + " nao existe"));
        return pedido.toDTO();
    }

    @Override
    public PedidoMonitorizacaoDTO save(PedidoMonitorizacaoDTO dto) {
        dto.realizado = false; // um pedido tem sempre o realizado a false
        PedidoMonitorizacao pedidoMonitorizacao = PedidoMonitorizacao.fromDTO(dto);
        return repo.save(pedidoMonitorizacao).toDTO();
    }

    @Override
    public Iterable<PedidoMonitorizacaoDTO> pedidosPorRealizar() {
        return repo.findPedidoMonitorizacaoPorRealizar().stream().map(PedidoMonitorizacao::toDTO).collect(Collectors.toList());
    }
}
